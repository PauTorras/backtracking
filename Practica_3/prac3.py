# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 21:31:31 2019

@author: Ruben Vera, Pau Torras, Laura Roldan
"""

import pandas as pd
import time


t1 = time.time()

db_tweets = pd.read_csv(filepath_or_buffer = "FinalStemmedSentimentAnalysisDataset.csv", delimiter = ";",
                        header = 0, names = ["tweetId","tweetText","tweetDate","sentimentLabel"])

db_tweets_NAN = db_tweets.dropna(thresh = 4)
del db_tweets_NAN['tweetDate']

#Per reduir els tweets negatius i positius per separat
reduccio_pos = 1
reduccio_neg = 0.9

db_good_tweets = db_tweets_NAN[db_tweets_NAN['sentimentLabel'] == 1]
db_good_tweets = db_good_tweets[0:(int(db_good_tweets.shape[0]*reduccio_pos))]
db_bad_tweets = db_tweets_NAN[db_tweets_NAN['sentimentLabel'] == 0]
db_bad_tweets = db_bad_tweets[0:(int(db_bad_tweets.shape[0]*reduccio_neg))]

good_tweets = db_good_tweets.shape[0]
bad_tweets = db_bad_tweets.shape[0]

k = 0.85 #percentatge de train
k_laplace = 1 #factor Laplace smoothing
div_index_good = int(good_tweets*k)
div_index_bad = int(bad_tweets*k)

db_tweets_train_NAN = db_good_tweets[0:div_index_good-1]
db_tweets_train_NAN = pd.concat([db_tweets_train_NAN, db_bad_tweets[0:div_index_bad-1]])
db_tweets_val_NAN = db_good_tweets[div_index_good:]
db_tweets_val_NAN = pd.concat([db_tweets_val_NAN, db_bad_tweets[div_index_bad:]])

t2 = time.time() - t1


#########################################################################
#
#                 Hold-out
#
#########################################################################



##########################################
# Diccionaris Training
##########################################
prob_paraula = {}

for index, tweet in db_tweets_train_NAN.iterrows():
    if tweet['sentimentLabel'] == 1:
        paraules =  tweet['tweetText'].split()
        for paraula in paraules:
            if paraula in prob_paraula:
                prob_paraula[paraula][0] += 1
            else:
                prob_paraula[paraula] = [1, 0]
                
    else:
        paraules =  tweet['tweetText'].split()
        for paraula in paraules:
            if paraula in prob_paraula:
                prob_paraula[paraula][1] += 1
            else:
                prob_paraula[paraula] = [0, 1]
            
        
keys = []
for key, value in prob_paraula.items():
    if ((value[0] + value[1]) == 1) or (value[0] == value[1]):
        keys.append(key)

for key in keys:
    del prob_paraula[key]        

t3 = time.time() - t2 -t1


############################################
# Diccionaris Validacio
############################################
TP = 0
FP = 0
FN = 0
TN = 0
p_y = (good_tweets+k_laplace)/(bad_tweets+good_tweets+k_laplace*2)
p_n = (bad_tweets+k_laplace)/(bad_tweets+good_tweets+k_laplace*2)

for index, tweet in db_tweets_val_NAN.iterrows():
    paraules = tweet['tweetText'].split()
    prob_y = p_y
    prob_n = p_n
    for paraula in paraules:
        if paraula in prob_paraula:
            total = prob_paraula[paraula][0]+prob_paraula[paraula][1]+k_laplace*2
            prob_y = (prob_y * ( (prob_paraula[paraula][0]+k_laplace) / total))
            prob_n = (prob_n * ( (prob_paraula[paraula][1]+k_laplace) / total))
            
    if prob_y > prob_n:
        if tweet['sentimentLabel'] == 1:
            TP += 1
        else:
            FP += 1
    else:
        if tweet['sentimentLabel'] == 0:
            TN += 1
        else:
            FN += 1
            
accuracy = (TP+TN)/(TP+FP+TN+FN)
precision = TP / (TP+FP)
recall = TP / (TP+FN)
specificity = TN / (TN+FP)

print("ACCURACY = ", accuracy)
print("PRECISION = ", precision)
print("RECALL = ", recall)
print("SPECIFICITY = ", specificity)

    
t4 = time.time() - t3 - t2 - t1

print("Creació diccionaris: ", t2, "Training: ", t3, "Validació: ", t4)



  
