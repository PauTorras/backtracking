# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 21:31:31 2019

@author: Ruben Vera, Pau Torras, Laura Roldan
"""

from copy import deepcopy
import pandas as pd
import numpy as np
import os
import math


pd.set_option('mode.chained_assignment', None)


#Llegim la base de dades separades en comes
mushroom_D = pd.read_csv('agaricus-lepiota.data', names=['class','cap-shape', 
'cap-surface', 'cap-color','bruises','odor','gill-attachment','gill-spacing',
'gill-size','gill-color','stalk-shape','stalk-root','stalk-surface-above-ring',
'stalk-surface-below-ring','stalk-color-above-ring','stalk-color-below-ring',
'veil-type','veil-color','ring-number','ring-type','spore-print-color',
'population','habitat'])

#Per si volem utilitzar numpy i no dataframe
mushroom_N = mushroom_D.rename_axis('ID').values


def entropy(n, labels):
    ent = 0
    for label in labels.keys():
        p_x = labels[label] / n
        ent += - p_x * math.log(p_x, 2)
    return ent


def unique(list1):
    unique_list = [] 

    for x in list1:
        if x not in unique_list:
            unique_list.append(x) 
    return unique_list


def ID3(mushroom_D):
    
    cols = list(mushroom_D)
    Node_attributes = {}

    for atribut in cols:
        Node_attributes[atribut] = unique(mushroom_D[atribut])


        
 