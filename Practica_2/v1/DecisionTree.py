import pandas as pd
import numpy as np
import os
import math


class DecisionTree:
    """
    Classe que conté l'arbre de decisió format.

    Attributes
    ----------
    max_depth : int
        Profunditat màxima de l'arbre. None en cas de no considerar-se.
    root : DecisionNode
        DecisionNode arrel de l'arbre.
    train_set : array_like
        Conjunt de dades sobre les quals entrenar
    ts_rows : int
        Nombre de files
    ts_cols : int
        Nombre de columnes

    Methods
    -------
    """
    def __init__(self, train_set, max_depth = None):
        self.train_set = train_set
        if len(train_set.shape) > 1:
            self.ts_rows = train_set.shape[0]
            self.ts_cols = train_set.shape[1]
        else:
            print("Només s'ha passat una fila de dades. No es pot fer classificació")
            self.ts_rows = None
            self.ts_cols = None
        self.max_depth = max_depth
        self.root = None

    def __str__(self):
        return "Arbre de decisio: \n" + str(self.root)

    def fit(self):
        self.root = DecisionNode(self.train_set, [x for x in range(self.ts_cols - 1)], 0, self.max_depth)
        self.root.treeGrowing()

    def predict(self, data):
        # Comprovar dimensionalitat de les dades
        # Predir fent servir l'arbre
        pass


class DecisionNode:
    """
    Classe que identifica un atribut de la base de dades en un node de decisió.

    Attributes
    ----------
    leaf : bool
        True si el node és un node fulla.
    attribute : str
        Nom del DecisionNode, és a dir, de la columna escollida.
    samples : Numpy array
        Conjunt de mostres que compleixen totes les classificacions fins al moment.
    entropy : double
        Entropia del DecisionNode.
    gini : double
        Gini del DecisionNode.
    children : list of DecisionNodes
        Llista d'obectes DecisionNode que són fills d'aquest node. N'hi ha tants
        com elements a children_labels.
    children_labels : list of str
        Etiqueta/nom que té cada aresta.
    ts_rows : int
        Nombre de files
    ts_cols : int
        Nombre de columnes
    divisibles : list of int
        Atributs pels quals encara no s'ha dividit la branca de l'arbre
    depth : int
        Nivell de profunditat del node actual
    depth_threshold : int
        Umbral màxim de profunditat admès

    Methods
    -------
    
    """
    def __init__(self, samples, divisibles = None, depth = None,d_thresh = None, leaf = False):
        self.leaf = leaf
        self.label = None
        self.attribute = None

        self.samples = samples
        self.ts_rows = self.samples.shape[0]
        self.ts_cols = self.samples.shape[1]

        self.divisibles = divisibles

        self.entropy = None
        self.gini = None

        self.children = [] 
        self.children_labels = []

        self.depth = depth
        self.depth_threshold = d_thresh

    def __str__(self):
        if not self.leaf:
            string = "---> NODE DECISIO Columna: " + str(self.attribute) + " | Distribucio: ( "
        else:
            string = "---> FULLA amb " + str(len(self.samples)) + " | Distribucio ( "
        unics = np.unique(self.samples[:,self.ts_cols - 1])
        for i in range(len(unics)):
            string += str(len(self.samples[self.samples[:, self.ts_cols - 1] == unics[i]])) + " (Classe " + str(unics[i]) + ") "
            string += " : "
        string += "TOTAL " + str(len(self.samples)) + " )\n"

        if not self.leaf:
            for child, label in zip(self.children, self.children_labels):
                string += ((self.depth + 1) * "\t") + "---( " + "{:<8}".format(str(label)[:8]) + " )" + str(child)

        return string


    def get_entropy(self, attr = None):
        
        entropy = 0
        rows_not_nans = self.ts_rows - np.isnan(self.samples[:, self.attribute])
        if attr is None:
            values = np.unique(self.samples[:,self.ts_cols - 1])

            for index in range(values.shape[0]):
                aux = self.samples[self.samples[:,self.ts_cols - 1] == values[index]]
                p_x = len(aux) / rows_not_nans
                entropy -= p_x * math.log2(p_x)
        elif 0 <= attr < self.ts_cols - 1:
            attr_values = np.unique(self.samples[:, attr])
            p_entropy = 0
            for value in range(attr_values.shape[0]):
                subset = self.samples[self.samples[:,attr] == attr_values[value]]

                values = np.unique(subset[:, self.ts_cols - 1])

                for index in range(values.shape[0]):
                    aux = subset[subset[:, self.ts_cols - 1] == values[index]]
                    p_x = len(aux) / len(subset)
                    p_entropy -= p_x * math.log2(p_x)
                entropy += (len(subset) / self.ts_rows) * p_entropy
        return entropy

    def treeGrowing(self):
        currentEntropy = self.get_entropy()
        maxGain = -float("inf")
        attrGain = -1

        if len(self.divisibles) > 0:
            for attr in self.divisibles:
                gain = currentEntropy - self.get_entropy(attr)

                if gain > maxGain:
                    maxGain = gain
                    attrGain = attr

            self.attribute = attrGain
            self.divisibles.remove(self.attribute)

            uniqueValues = np.unique(self.samples[:, self.attribute])
            for unique in range(len(uniqueValues)):
                self.children.append(DecisionNode(self.samples[self.samples[:, self.attribute] == unique], self.divisibles,
                                                  self.depth + 1, self.depth_threshold))
                self.children_labels.append(unique)
                self.children[-1].treeGrowing()

        else:
            self.leaf = True
            uniqueValues = np.unique(self.samples[:,self.ts_cols - 1])
            mostUnique = -1
            mostValue = -1
            for a in range(len(uniqueValues)):
                numUnique = len(self.samples[self.samples[:, self.ts_cols - 1] == uniqueValues[a]])
                if numUnique > mostUnique:
                    mostUnique = numUnique
                    mostValue = a
            self.label = mostValue


    def treePruning(self):
        pass