from DecisionTree import *
from copy import deepcopy
import pandas as pd
import numpy as np
import os
import math


pd.set_option('mode.chained_assignment', None)


#Llegim la base de dades separades en comes
mushroom_D = pd.read_csv('agaricus-lepiota.data', names=['class','cap-shape',
'cap-surface', 'cap-color','bruises','odor','gill-attachment','gill-spacing',
'gill-size','gill-color','stalk-shape','stalk-root','stalk-surface-above-ring',
'stalk-surface-below-ring','stalk-color-above-ring','stalk-color-below-ring',
'veil-type','veil-color','ring-number','ring-type','spore-print-color',
'population','habitat'])

#Per si volem utilitzar numpy i no dataframe
mushroom_N = mushroom_D.rename_axis('ID').values

mushroom_DT = DecisionTree(mushroom_N)
mushroom_DT.fit()
print(mushroom_DT)

############################################

arr = np.array([["test", 1],["hello", 2], ["test", 1]])

decisionnode = DecisionNode(arr)
decisionnode.depth = 0
decisionnode.children.append(DecisionNode(arr))
decisionnode.children_labels.append("hi")
decisionnode.children[0].depth = 1

decisionnode.children[0].children.append(DecisionNode(arr))
decisionnode.children[0].children_labels.append("tacatacatacatatada")
decisionnode.children[0].children[0].depth = 2

decisionnode.children[0].children.append(DecisionNode(arr))
decisionnode.children[0].children_labels.append("berry")
decisionnode.children[0].children[1].depth = 2

print(decisionnode)


