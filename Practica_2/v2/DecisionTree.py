"""
DECISION TREE
Pau Torras, Laura Roldan, Rubén Vera

Implementació de l'algorisme de ID3 / C4.5 amb un sistema de validació per agilitzar el procés de test dels resultats
"""


#import graphviz
import pandas as pd
import numpy as np
import os
import math
import copy as cp


"""
Càlcul del guany i de l'entropia
"""

def entropy(column):
    val_unic = column.unique()
    entropia = 0

    for i in range(len(val_unic)):
        p_x = len(column[column == val_unic[i]]) / len(column)
        entropia -= p_x * np.log2(p_x)

    return entropia

def entropy_NAN(column):
    val_unic = column.unique()
    val_unic = val_unic[pd.notnull(val_unic)]
    entropia = 0

    column_not_nans = len(column) - np.count_nonzero(pd.isnull(column))

    for i in range(len(val_unic)):
        p_x = len(column[column == val_unic[i]]) / column_not_nans
        entropia -= p_x * np.log2(p_x)

    return entropia

def gini(column):
    val_unic = column.unique()
    gini = 1

    for i in range(len(val_unic)):
        p_x = len(column[column == val_unic[i]]) / len(column)
        gini -= p_x ** 2

    return gini

def gini_NAN(column):
    val_unic = column.unique()
    val_unic = val_unic[pd.notnull(val_unic)]
    gini = 1

    column_not_nans = len(column) - np.count_nonzero(pd.isnull(column))

    for i in range(len(val_unic)):
        p_x = len(column[column == val_unic[i]]) / column_not_nans
        gini -= p_x ** 2

    return gini

def entropy_gain(x, y):
    current_ent = entropy(y)
    val_unic = x.unique()
    ent = 0

    for i in range(len(val_unic)):
        files_triades = y[x == val_unic[i]]
        ent += (len(files_triades) / len(y)) * entropy(files_triades)

    return current_ent - ent

def entropy_gain_NAN(x, y):
    current_entropy = entropy_NAN(y)
    val_unic = x.unique()
    val_unic = val_unic[pd.notnull(val_unic)]
    ent = 0

    column_not_nans = len(x) - np.count_nonzero(pd.isnull(x))

    for i in range(len(val_unic)):
        files_triades = y[x == val_unic[i]]
        ent += (len(files_triades) / column_not_nans) * entropy_NAN(files_triades)

    return (column_not_nans / len(y)) * (current_entropy - ent)

def gini_gain(x, y):
    current_gini = gini(y)
    val_unic = x.unique()
    g = 0

    for i in range(len(val_unic)):
        files_triades = y[x == val_unic[i]]
        g += (len(files_triades) / len(y)) * gini(files_triades)

    return current_gini - g

def gini_gain_NAN(x, y):
    current_gini = gini_NAN(y)
    val_unic = x.unique()
    val_unic = val_unic[pd.notnull(val_unic)]
    g = 0
        
    column_not_nans = len(x) - np.count_nonzero(pd.isnull(x))

    for i in range(len(val_unic)):
        files_triades = y[x == val_unic[i]]
        g += (len(files_triades) / column_not_nans) * gini_NAN(files_triades)

    return (column_not_nans / len(y)) * (current_gini - g)

class DecisionTree:
    """
    Arbre de decisió. Classe contenidora de la configuració, les dades i el model generat en conseqüència.

    Attributes
    ---------
    data : pandas.Dataframe
        Informació d'entrada
    target : pandas.Series
        Columna de la classe a predir
    max_size : int
        Mida màxima de l'arbre en profunditat
    split_criteria : str
        Mètrica d'homogeneitat a emprar per l'algorisme. Pot ser "entropy" (per defecte) o "gini"
    settings : dict
        Contenidor de les configuracions recurrents a tots els nodes
    """
    def __init__(self, data, target, max_size = None, split_criteria = "entropy"):
        self.data = data
        self.target = target

        self.settings = {"split_criteria": split_criteria, "max_size": max_size,
                         "target_values": list(target[pd.notnull(target)].unique()), 'max_unique'  : 10}

        assert self.settings["split_criteria"] in ["entropy", "gini"], "Mètode de divisió no vàlid"
        assert len(self.settings["target_values"]) > 1, "La classe de sortida només té un element"

        assert len(target) == len(data), "Diferència en el nombre de files entre target i data"

        self.root = None

    def __str__(self):
        return self.root.stringify()

    def fit(self):
        """
        Genera un model d'arbre de decisió

        Returns
        -------
        None
        """
        self.root = DecisionNode(self.data, self.target, 0, None, self.settings)
        self.root.tree_growing()

    def predict_single(self, x):
        """
        Prediu l'etiqueta del conjunt de sortida en base a una sola fila de dades

        Parameters
        ----------
        x : pandas.Series
            Fila sobre la qual predir

        Returns
        -------
        object
            Etiqueta del valor que li correspon a la mostra
        """
        assert len(x) == self.data.shape[1], "Valors de mida diferent"

        aux = self.root

        while len(aux.children) > 0:
            if pd.notnull(x.loc[aux.attribute]):
                found = False
                index = 0
                while not found and index < len(aux.children):
                    if aux.children[index].label == x.loc[aux.attribute]:
                        found = True
                        aux = aux.children[index]
                    else:
                        index += 1

                assert found, "Valor no existent a les etiquetes"
            else:
                probs = self.get_probability(x, aux)

                max = 0.0
                keymax = None

                for key, value in probs.items():
                    if value > max:
                        max = value
                        keymax = key

                return keymax

        return aux.get_most()

    def get_probability(self, x, node):
        """
        Retorna la probabilitat de pertànyer a les classes de sortida. S'utilitza com a suport del classificador normal
        (més ràpid) per classificar casos amb valors nuls al conjunt d'entrada.

        Parameters
        ----------
        x : pandas.Series
            Tupla a ser classificada
        node : DecisionNode
            Referència del node que ha de classificar la mostra

        Returns
        -------
        dict
            Diccionari amb les probabilitats de pertànyer a cadascuna de les classes de sortida
        """
        if len(node.children) == 0:
            suma = sum(node.target_values.values())
            return { clau : valor / suma for clau, valor in node.target_values.items()}
        else:
            value = x.loc[node.attribute]
            if pd.notnull(value):
                for i in node.children:
                    if i.label == value:
                        return self.get_probability(x, i)
                x.loc[node.attribute] = np.NaN
                return self.get_probability(x, node)
            else:
                aux = {key : 0 for key in node.target_values.keys()}
                for i in node.children:
                    childict = self.get_probability(x, i)
                    for key in aux.keys():
                        aux[key] += childict[key] * (sum(i.target_values.values()) / sum(node.target_values.values()))
                return aux

    def predict(self, x):
        """
        Prediu el resultat d'un conjunt de mostres en base al model trobat

        Parameters
        ----------
        x : pandas.Dataframe
            Conjunt de mostres a predir

        Returns
        -------
        np.array
            Predicció amb les etiquetes del conjunt de sortida corresponents.
        """
        output = []
        for i in range(len(x)):
            output.append(self.predict_single(x.iloc[i]))

        return np.array(output)


class DecisionNode:
    """
    Implementació d'un arbre de decisió ID3 (guany d'entropia/gini sense normalitzar). Node de decisió intermig

    Attributes
    ----------
    data : pandas.Dataframe
        Conjunt de dades d'entrada
    target : pandas.Series
        Columna de dades a predir
    depth : int
        Profunditat del node actual
    label : object
        Etiqueta del valor del node pare pel qual s'escullen les mostres del node actual
    settings : dict
        Referència al diccionari de configuració de l'arbre de decisió
    attribute : object
        Índex de la columna per la qual es divideix l'espai de mostres
    threshold : float
        Valor de divisió d'atributs continus
    label : object
        Etiqueta otorgada a les mostres de la decisió superior
    split_value : float
        Valor de gini o entropia del node
    children : list
        Llista de nodes de decisió fills
    target_values : dict
        Diccionari amb el nombre de mostres de cada tipus del conjunt de sortida al node actual
    """
    def __init__(self, data, target, depth, label, settings):
        self.data = data
        self.target = target
        self.settings = settings
        self.depth = depth


        self.attribute = None
        self.threshold = None # Valor de divisió en cas de continu
        self.label = label
        self.split_value = 0.0

        self.children = []

        # Diccionari amb les estadistiques de sortida
        self.target_values = { x : 0 for x in settings["target_values"]}


    def __str__(self):
        if self.attribute is None:
            return "|---"*self.depth + "Node Fulla | Etiqueta: " + str(self.label) + " | Distribució y: " + \
                str(self.target_values) + " | " + self.settings["split_criteria"] + " -> " + str(self.split_value) + "\n"
        else:
            return "|---"*self.depth + "Etiqueta: " + str(self.label) + " | Atribut: " + str(self.attribute) + \
                   " | Distribució y: " + str(self.target_values) + " | " + self.settings["split_criteria"] + " -> " + \
                   str(self.split_value) + "\n"

    def stringify(self):
        """
        Representa el node actual i els seus fills en forma de string

        Returns
        -------
        str
            Representació de l'arbre
        """
        base = str(self)

        for nod in self.children:
            base += nod.stringify()

        return base

    def get_most(self):
        """
        Retorna l'etiqueta més freqüent del node en qüestió

        Returns
        -------
        object
            Índex del pandas.DataFrame de la columna en qüestió
        """
        max_value = 0
        max_key = None
        for key, value in self.target_values.items():
            if value > max_value:
                max_key = key
                max_value = value
        return max_key

    def is_discrete(self, attr):
        """
        Determina si la columna passada com a paràmetre conté un valor discret o no
        Parameters
        ----------
        attr : object
            Número o String de l'índex de la columna

        Returns
        -------
        bool
            Si la columna és discreta o no
        """
        assert attr in list(self.data), "Atribut no present a la base de dades"

        data_type = self.data.dtypes[attr]

        if data_type is np.int or data_type is np.float:
            # Mètode relatiu
            # if len(self.data.loc[:,attr].unique()) / len(self.data.loc[:,attr]) <= 0.05:
            if len(self.data.loc[:,attr].unique()) > self.settings["max_unique"]:
                return False
        return True

    def tree_growing(self):
        """
        Implementació de l'algorisme de Tree Growing (ID3) segons els paràmetres de la classe DecisionNode

        Returns
        -------
            None
        """
        if self.settings["split_criteria"] == 'entropy':
            self.split_value = entropy_NAN(self.target)
        elif self.settings["split_criteria"] == 'gini':
            self.split_value = gini_NAN(self.target)

        for i in self.target_values.keys():
            self.target_values[i] = len(self.target[self.target == i])

        # Condicions de parada
        if self.depth >= self.settings["max_size"] or self.split_value == 0.0:
            pass
        # Algorisme
        else:
            # Trobar atribut amb millor split
            best_column = None
            best_value = 0.0
            value = 0.0
            best_threshold = 0.0

            for column in list(self.data):
                if self.is_discrete(column):
                    # Cas discret
                    if self.settings["split_criteria"] == 'entropy':
                        value = entropy_gain_NAN(self.data.loc[:, column], self.target)
                    elif self.settings["split_criteria"] == 'gini':
                        value = gini_gain_NAN(self.data.loc[:, column], self.target)
                else:
                    # Cas continu
                    pass

                if value > best_value:
                    best_value = value
                    best_column = column

            if best_column is not None:
                self.attribute = best_column

                if self.is_discrete(best_column):
                    valors_unics = self.data[best_column].unique()
                    valors_unics = valors_unics[pd.notnull(valors_unics)]
                    for valor_unic in range(len(valors_unics)):
                        index = self.data.loc[:, best_column] == valors_unics[valor_unic]
                        index = np.logical_and(pd.notnull(self.target), index)
                        info = self.data[index]
                        info.drop(best_column, axis=1)

                        self.children.append(
                            DecisionNode(info, self.target[index], self.depth + 1, valors_unics[valor_unic],
                                         self.settings))

                    del self.data
                    del self.target

                    for child in self.children:
                        child.tree_growing()

                else:
                    pass


class Validator:
    """
    Contenidor d'un decision tree per automatitzar el procés de test i mostrar resultats

    Attributes
    ----------
    x_data : pandas.DataFrame
        Conjunt de dades d'entrada
    y_data : pandas.Series
        Columna de dades de sortida
    max_tree_size : int
        Mida màxima de l'arbre de decisió generat
    split_method : str
        Mètrica per calcular la divisió òptima del conjunt en qüestió. Pot ser "gini" o "entropy" (per defecte)
    val_method : str
        Mètode de validació. Pot ser "holdout" (per defecte) o "k-fold"
    val_k : number
        Quan el mètode sigui holdout es considera que val_k és la proporció de mostres del training set respecte a les
        de test. Quan sigui k-fold, es considera que és la K.
    confusio : numpy.array-like
        Matriu de confusió computable
    fancy : pandas.Dataframe
        Matriu maca per output
    accuracy : float
        Mètrica d'accuracy
    recall : float
        Mètrica de recall
    precision : float
        Mètrica de precision

    """
    def __init__(self, x_data, y_data, max_tree_size = None, split_method = "entropy", val_method = "holdout", val_k = 0.8):
        assert len(x_data) == len(y_data), "Diferència en el nombre de files entre target i data"
        self.x_data = x_data
        self.y_data = y_data

        self.max_tree_size = max_tree_size
        self.split_method = split_method


        assert val_method in ["k-fold", "holdout"], "Mètode no vàlid. Utilitzar 'k-fold' o 'holdout'"
        self.val_method = val_method
        self.val_k = val_k

        self.decision_tree = None
        self.confusio = None
        self.fancy = None

        self.accuracy = 0.0
        self.recall = 0.0
        self.precision = 0.0


    def validate(self):
        """
        Funció de validació. Segons el mètode de validació escollit,
        durà a terme k-fold cross-validation o un holdout

        Returns
        -------
        None
        """
        self.shuffle_dataset()

        self.accuracy = 0.0
        self.recall = 0.0
        self.precision = 0.0

        if self.val_method == "holdout":
            assert 0.0 < self.val_k <= 1.0, "Valor " + str(self.val_k) + " fora del rang [0, 1] per k-fold"

            div_index = int(len(self.x_data) * self.val_k)

            train_x = self.x_data.iloc[0:div_index - 1]
            train_y = self.y_data.iloc[0:div_index - 1]

            test_x = self.x_data.iloc[div_index:].reset_index(drop=True)
            test_y = self.y_data.iloc[div_index:].reset_index(drop=True)

            self.decision_tree = DecisionTree(train_x, train_y, self.max_tree_size, self.split_method)
            self.decision_tree.fit()

            prediction = pd.Series(self.decision_tree.predict(test_x))

            self.confusio = np.array(pd.crosstab(test_y, prediction))
            self.fancy = pd.crosstab(test_y, prediction)

            for i in range(self.confusio.shape[0]):
                self.precision += self.confusio[i, i] / (np.sum(self.confusio[:, i]) * self.confusio.shape[0])
                self.recall += self.confusio[i, i] / (np.sum(self.confusio[i]) * self.confusio.shape[0])

            diag = np.diag_indices(self.confusio.shape[0])
            self.accuracy = np.sum(self.confusio[diag]) / np.sum(self.confusio)

        elif self.val_method == "k-fold":
            assert 0 < self.val_k < len(self.x_data), "Valor " + str(self.val_k) + " fora del rang"
            
            for k in range(self.val_k):
                index = np.arange(len(self.x_data))
                index = np.logical_and((index >= (len(self.x_data) * (k /self.val_k))),
                                       (index < (len(self.x_data) * ((k + 1) /self.val_k))) )

                train_x = self.x_data[np.logical_not(index)]
                train_y = self.y_data[np.logical_not(index)]

                test_x = self.x_data[index].reset_index(drop=True)
                test_y = self.y_data[index].reset_index(drop=True)
                    
                self.decision_tree = DecisionTree(train_x, train_y, self.max_tree_size, self.split_method)
                self.decision_tree.fit()

                prediction = pd.Series(self.decision_tree.predict(test_x))

                self.confusio = np.array(pd.crosstab(test_y, prediction))
                self.fancy = pd.crosstab(test_y, prediction)

                for i in range(self.confusio.shape[0]):
                    self.precision += self.confusio[i,i] / np.sum(self.confusio[:,i])
                    self.recall += self.confusio[i,i] / np.sum(self.confusio[i])

            self.precision /= (self.val_k * self.confusio.shape[0])
            self.recall /= (self.val_k * self.confusio.shape[0])
            diag = np.diag_indices(self.confusio.shape[0])
            self.accuracy = np.sum(self.confusio[diag]) / np.sum(self.confusio)
            

    def shuffle_dataset(self):
        """
        Reordena el conjunt de dades (self.[x|y]_data) de manera aleatòria.

        Returns
        -------
        None
        """
        newindex = np.random.permutation(self.x_data.index)

        self.x_data = (self.x_data.reindex(newindex)).reset_index(drop=True)
        self.y_data = (self.y_data.reindex(newindex)).reset_index(drop=True)

    def summary(self):
        print("Mètode: ", self.val_method, " | K: ", self.val_k)
        print("Resum de mètriques:")
        print("ACCURACY: ", self.accuracy)
        print("PRECISION: ", self.precision)
        print("RECALL: ", self.recall)

        print("Última matriu de confusió computada:")
        print(self.fancy)
        print("Últim model obtingut:")
        print(self.decision_tree)