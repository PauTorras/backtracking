# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 21:31:31 2019

@author: Ruben Vera, Pau Torras, Laura Roldan
"""

from copy import deepcopy
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import os
import math
import time

import DecisionTree as dt


def plot_results(res, filename, header):
    for key in res.keys():
        if key != "mxdepth":
            plt.plot(np.array(res["mxdepth"]), np.array(res[key]), c='b')
            plt.xlabel("Profunditat permesa")
            plt.ylabel(key)
            plt.title(header)

            plt.savefig(filename + "_" + key + ".png")
            plt.show()


pd.set_option('mode.chained_assignment', None)

#Llegim la base de dades separades en comes
mushroom_D = pd.read_csv('agaricus-lepiota.data', names=['class','cap-shape',
'cap-surface', 'cap-color','bruises','odor','gill-attachment','gill-spacing',
'gill-size','gill-color','stalk-shape','stalk-root','stalk-surface-above-ring',
'stalk-surface-below-ring','stalk-color-above-ring','stalk-color-below-ring',
'veil-type','veil-color','ring-number','ring-type','spore-print-color',
'population','habitat'])

########################################################################################################################
# Prova 1
########################################################################################################################

results_t1 = {"accuracy" : [], "recall" : [], "precision" : [], "mxdepth" : [], "time" : []}

for mxdepth in range(2,12):
    tb = time.time()
    valid_mush = dt.Validator(mushroom_D.loc[:,'cap-shape':'habitat'], mushroom_D.loc[:,'class'], mxdepth, 'entropy', 'k-fold', 5)
    valid_mush.validate()
    ts = time.time() - tb

    valid_mush.summary()


    results_t1["accuracy"].append(valid_mush.accuracy)
    results_t1["recall"].append(valid_mush.recall)
    results_t1["precision"].append(valid_mush.precision)
    results_t1["time"].append(ts)
    results_t1["mxdepth"].append(mxdepth)

plot_results(results_t1, "./grafics/prova1", "Prova d'adaptació a la profunditat permesa (entropia, 5-fold)")


########################################################################################################################
# Prova 2
########################################################################################################################

results_t2 = {"accuracy" : [], "recall" : [], "precision" : [], "mxdepth" : [], "time" : []}

for mxdepth in range(2,12):
    tb = time.time()
    valid_mush = dt.Validator(mushroom_D.loc[:,'cap-shape':'habitat'], mushroom_D.loc[:,'class'], mxdepth, 'gini', 'k-fold', 5)
    valid_mush.validate()
    ts = time.time() - tb

    valid_mush.summary()

    results_t2["accuracy"].append(valid_mush.accuracy)
    results_t2["recall"].append(valid_mush.recall)
    results_t2["precision"].append(valid_mush.precision)
    results_t2["time"].append(ts)
    results_t2["mxdepth"].append(mxdepth)

plot_results(results_t2, "./grafics/prova2", "Prova d'adaptació a la profunditat permesa (gini, 5-fold)")

########################################################################################################################
# Prova 3
########################################################################################################################

mushroom_NAN = mushroom_D.copy(deep = True)

mask = np.random.random(mushroom_NAN.shape)<0.2
mushroom_NAN[mask] = np.nan

results_t3 = {"accuracy" : [], "recall" : [], "precision" : [], "mxdepth" : [], "time" : []}

for mxdepth in range(2,12):
    tb = time.time()
    valid_nan = dt.Validator(mushroom_NAN.loc[:, 'cap-shape':'habitat'], mushroom_NAN.loc[:, 'class'], mxdepth, 'entropy',
                             'k-fold', 5)
    valid_nan.validate()
    ts = time.time() - tb

    valid_nan.summary()

    results_t3["accuracy"].append(valid_nan.accuracy)
    results_t3["recall"].append(valid_nan.recall)
    results_t3["precision"].append(valid_nan.precision)
    results_t3["time"].append(ts)
    results_t3["mxdepth"].append(mxdepth)

plot_results(results_t3, "./grafics/prova3", "Prova amb ~20% NaN sobre mushrooms (entropia, 5-fold)")

########################################################################################################################
# Prova 4
########################################################################################################################

mushroom_NAN = mushroom_D.copy(deep = True)

mask = np.random.random(mushroom_NAN.shape)<0.5
mushroom_NAN[mask] = np.nan

results_t4 = {"accuracy" : [], "recall" : [], "precision" : [], "mxdepth" : [], "time" : []}

for mxdepth in range(2,12):
    tb = time.time()
    valid_nan = dt.Validator(mushroom_NAN.loc[:, 'cap-shape':'habitat'], mushroom_NAN.loc[:, 'class'], mxdepth, 'entropy',
                             'k-fold', 5)
    valid_nan.validate()
    ts = time.time() - tb

    valid_nan.summary()

    results_t4["accuracy"].append(valid_nan.accuracy)
    results_t4["recall"].append(valid_nan.recall)
    results_t4["precision"].append(valid_nan.precision)
    results_t4["time"].append(ts)
    results_t4["mxdepth"].append(mxdepth)

plot_results(results_t4, "./grafics/prova4", "Prova amb ~50% NaN sobre mushrooms (entropia, 5-fold)")



# chessgames_D = pd.read_csv("games_clean.csv", names=['rated', 'created_at', 'last_move_at', 'turns', 'winner',
#                                                      'increment_code', 'white_rating', 'black_rating', 'opening_eco',
#                                                      'opening_name', 'opening_ply'], sep=';', skiprows=2)
#
#
# chessgames_DISCRETE = chessgames_D.loc[:,['rated', 'opening_name', 'increment_code','opening_eco', 'winner']]
#
# valid_chess =  dt.Validator(chessgames_DISCRETE.loc[:,['rated', 'opening_name', 'increment_code','opening_eco']],
#                             chessgames_DISCRETE.loc[:,'winner'], 3, 'entropy', 'holdout', 0.5)
# valid_chess.validate()
# valid_chess.summary()