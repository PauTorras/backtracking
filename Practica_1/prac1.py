"""
Practica 1: Mots encreuats
    per Pau Torras, Rubén Vera i Laura Roldán

Emplaça amb l'algorisme de backtracking paraules d'un diccionari en un 
tauler de mots encreuats.

Versio amb Numpy
"""

# Biblioteques externes
import numpy as np
import pandas as pd

# Col·lecció estàndard de Python
import time
import random
from copy import deepcopy


class Paraula:
    """
    Classe que identifica una paraula del tauler de mots encreuats.

    Attributes
    ----------
    row : int
        Fila on comença la paraula.
    col : int
        Columna on comença la paraula.
    length : int
        Longitud de la paraula.
    orient : str
        Direccio de la paraula. Si 'h' la paraula es horitzontal. Si 'v',
        vertical.

    Methods
    -------
    buildDomini : None
        Construeix el domini de possibles paraules que hi pot haver en aquesta
        paraula
    updateDomini : None
        Modifica el domini per acomodar-se a noves restriccions, NO per
        fer backtracking
    """
    def __init__(self, row, col, length, orient, domini = None):
        self.row = row
        self.col = col
        self.length = length
        self.orient = orient
        self.domini = domini
    
    def __str__(self):
        return 'Paraula amb | f: ' + str(self.row) + ' \t | c: ' + str(self.col) \
            + ' \t | len: ' + str(self.length) + ' \t | o: ' + self.orient


    def buildDomini(self, tauler, diccionari):
        """
        Funcio que crea el domini de possibles solucions de la paraula que
        fa la crida

        Parameters
        ----------
        tauler : np.array
            Matriu numpy que representa el tauler de joc
        diccionari : dict
            Diccionari de totes les paraules possibles on la clau es la 
            longitud de la paraula i el contingut es un numpy array amb les 
            paraules de tal longitud.

        Returns
        -------
        None
        """
        continguts = None

        if self.orient == 'h':
            continguts = tauler[self.row, self.col : self.col + self.length]

        elif self.orient == 'v':
            continguts = tauler[self.row : self.row + self.length, self.col]

        lletres = np.char.not_equal(continguts, np.full(continguts.shape, '0'))

        paraules = np.char.equal( diccionari[self.length][:,lletres], continguts[lletres])
        paraules = np.all(paraules, axis=1)

        self.domini = diccionari[self.length][paraules]
    

    def updateDomini(self, tauler):
        """
        Funcio que restringeix el domini de possibles solucions de la paraula 
        que fa la crida (nomes aplicable quan apareixen noves restriccions)

        Parameters
        ----------
        tauler : np.array
            Matriu numpy que representa el tauler de joc

        Returns
        -------
        None
        """
        continguts = None

        if self.domini is not None:
            if self.orient == 'h':
                continguts = tauler[self.row, self.col : self.col + self.length]

            elif self.orient == 'v':
                continguts = tauler[self.row : self.row + self.length, self.col]
            
            lletres = np.char.not_equal(continguts, np.full(continguts.shape, '0'))

            paraules = np.char.equal( self.domini[:,lletres], continguts[lletres])
            paraules = np.all(paraules, axis=1)

            self.domini = self.domini[paraules]
        
    def eliminaParaula(self, paraulaStr):
        if self.domini is not None:
            self.domini = self.domini[np.logical_not(np.all(self.domini == paraulaStr, axis=1))]


def llegirDiccionari(nomFitxer):
    """ 
    Funció que crea un objecte diccionari a partir d'un fitxer

    Parameters
    ----------
    nomFitxer : str
        Nom de l'arxiu diccionari
    
    Returns
    -------
    dict
        Diccionari d'arrays amb totes les paraules endreçades segons longitud
    """
    diccionari = {}
    output = {}

    fitxer = open(nomFitxer, "r")
    

    for line in fitxer:
        line = line.rstrip()
        if len(line) not in diccionari.keys():

            #FIXME: No trobo manera mes eficient de passar-ho a array de chars
            diccionari[len(line)] = [np.array(list(line), dtype=np.str)]
        else:
            diccionari[len(line)].append(np.array(list(line), dtype=np.str))
    
    for clau in diccionari.keys():
        output[clau] = np.array(diccionari[clau], dtype=np.str)
    
    return output

def fastLlegirDiccionari(nomFitxer):
    fitxer = open(nomFitxer)
    diccionari = {}
    #
    data = pd.read_csv(fitxer, dtype = object, encoding='utf-8').to_numpy(dtype=np.array_str)
    length = np.vectorize(len)(data)

    maxLen = np.amax(length)

    for i in range(2, maxLen + 1):
        diccionari[i] = np.asarray(data[length == i])

    return diccionari

def reduirDiccionari(diccionari, factor):
    """
    Funció que redueix la mida del diccionari en el factor proporcionat com a 
    paràmetre

    Parameters
    ----------
    diccionari : dict
        Diccionari amb claus longitud de les paraules i valors les paraules
    factor : int
        Factor de reduccio del diccionari

    Returns
    -------
    dict
        Diccionari modificat
    """
    for longitud in diccionari.keys():
        diccionari[longitud] = diccionari[longitud][np.arange(start=0 ,stop=diccionari[longitud].shape[0],step=factor)]
    return diccionari


def llegirTauler(nomFitxer):
    """ 
    Funció que crea un tauler com a matriu 2D a partir d'un fitxer

    Parameters
    ----------
    nomFitxer : str
        Nom de l'arxiu on hi ha el tauler
    
    Returns
    -------
    list
        Numpy Array de dues dimensions on '0' es casella buida i '#' casella
        fosca
    """
    fitxer = open(nomFitxer)
    llista = []
    for line in fitxer:
        llista.append([char for char in line.split()])
    return np.array(llista, dtype=np.str)


def identificarParaules(tauler):
    """
    Funció que ubica la posició de les paraules, la seva longitud i la
    orientació en un tauler de Mots Encreuats

    Parameters
    ----------
    tauler : list
        Matriu numpy amb '0' a les caselles buides i '#' a les caselles fosques

    Returns
    -------
    list
        Llista de Paraules
    """
    paraules = []

    # Paraules Horitzontals
    for indexFila in range(tauler.shape[0]):
        cInici = -1
        for indexColumna in range(tauler.shape[1]):
            if cInici >= 0 and tauler[indexFila][indexColumna] == '#':
                paraules.append(Paraula(indexFila,cInici, indexColumna - cInici, 'h'))
                cInici = -1
            if cInici == -1 and tauler[indexFila][indexColumna] != '#':
                cInici = indexColumna
        if cInici >= 0:
            paraules.append(Paraula(indexFila,cInici, tauler.shape[1] - cInici, 'h'))
    
    # Paraules Verticals
    for indexColumna in range(tauler.shape[1]):
        fInici = -1
        for indexFila in range(tauler.shape[0]):
            if fInici >= 0 and tauler[indexFila][indexColumna] == '#':
                paraules.append(Paraula(fInici, indexColumna,indexFila - fInici, 'v'))
                fInici = -1
            if fInici == -1 and tauler[indexFila][indexColumna] != '#':
                fInici = indexFila
        if fInici >= 0:
            paraules.append(Paraula(fInici, indexColumna, tauler.shape[0] - fInici, 'v'))
            
    return [paraula for paraula in paraules if paraula.length > 1]


def assignaParaula(idParaula, strParaula, tauler):
    """
    Funció que emplaça una paraula strParaula sobre el tauler. Sobreescriu
    totes les caselles

    Parameters
    ----------
    idParaula : Paraula
        Objecte de la struct paraula que la caracteritza
    strParaula : np.array
        Numpy array amb la paraula
    tauler : list
        Matriu tauler

    Returns
    -------
    np.array
        Matriu numpy tauler amb la nova paraula
    """
    if idParaula.orient == 'h':
        tauler[idParaula.row, idParaula.col : idParaula.col + idParaula.length] = strParaula
    if idParaula.orient == 'v':
        tauler[idParaula.row : idParaula.row + idParaula.length, idParaula.col] = strParaula

    return tauler


def extreureParaula(tauler, paraula):
    """
    Funció que extreu el numpy array de caràcters del tauler especificat per
    una paraula

    Parameters
    ----------
    tauler : np.array
        Tauler de joc representat per una matriu numpy
    paraula : Paraula
        Objecte que representa una paraula

    Returns
    -------
    np.array
        Array numpy de caràcters amb el contingut del tauler
    """
    if paraula.orient == 'v':
        return tauler[paraula.row : paraula.row + paraula.length, paraula.col]
    else:
        return tauler[paraula.row, paraula.col : paraula.col + paraula.length]


#TODO Metode mes maco :c
def trobarInterseccions(paraula, llistaParaules, tauler):
    """
    Funció que troba totes les paraules que interseccionen amb una paraula
    concreta.

    Parameters
    ----------
    idParaula : Paraula
        Objecte de la classe paraula que la caracteritza
    llistaParaules : list
        Llista de classes Paraula amb totes les que existeixen sobre el tauler
    tauler : list
        Matriu tauler

    Returns
    -------
    list
        Llista de Paraules que interseccionen
    """
    interseccions = []
    if paraula.orient == 'h':
        for columna in range(paraula.col, paraula.col + paraula.length):
            found = False
            fila = paraula.row
            while not found and fila >= 0:
                if tauler[fila][columna] != '#':
                    fila -= 1
                else:
                    found = True
            if not found:
                fila = 0
            else:
                fila += 1
            found = False
            iterator = 0
            while not found and iterator < len(llistaParaules):
                if llistaParaules[iterator].col == columna and llistaParaules[iterator].row == fila:
                    interseccions.append(llistaParaules[iterator])
                    found = True
                else:
                    iterator += 1
    
    elif paraula.orient == 'v':
        for fila in range(paraula.row, paraula.row + paraula.length):
            found = False
            columna = paraula.col
            while not found and columna >= 0:
                if tauler[fila][columna] != '#':
                    columna -= 1
                else:
                    found = True
            if not found:
                columna = 0
            else:
                columna += 1
            found = False
            iterator = 0
            while not found and iterator < len(llistaParaules):
                if llistaParaules[iterator].col == columna and llistaParaules[iterator].row == fila:
                    interseccions.append(llistaParaules[iterator])
                    found = True
                else:
                    iterator += 1
    return interseccions

#DEPRECATED
def oldSortParaules(llistaParaules, tauler):
    sortedParaules = []
    maxParaula = Paraula(-1,-1,-1,'h')
    for paraula in llistaParaules:
        if paraula.length > maxParaula.length:
            maxParaula = paraula
    sortedParaules.append(maxParaula)

    index = 0
    while index < len(sortedParaules):
        for paraula in trobarInterseccions(sortedParaules[index], llistaParaules, tauler):
            if paraula not in sortedParaules:
                sortedParaules.append(paraula)
        index += 1

    if len(llistaParaules) != len(sortedParaules):
        noPresents = [paraula for paraula in llistaParaules if paraula not in sortedParaules]
        sortedParaules += noPresents

    return sortedParaules

def sortParaules(llistaParaules, tauler):
    """
    Ordena les paraules de manera que fer forward checking serà més eficient

    Parameters
    ----------
    llistaParaules : list
        Llista d'elements Paraula a ordenar
    tauler : np.array
        Matriu numpy que representa el tauler de joc
    
    Returns
    -------
    list
        Llista amb les paraules ordenades
    """

    sorted_paraules = []

    llistaParaules.sort(key=lambda a:len(trobarInterseccions(a, llistaParaules,tauler)))

    sorted_paraules.append(llistaParaules[0])

    recSortParaules(sorted_paraules, llistaParaules, tauler)
    missed = [paraula for paraula in llistaParaules if paraula not in sorted_paraules]
    return sorted_paraules + missed


def recSortParaules(sorted_paraules, paraules, tauler):
    intersect = trobarInterseccions(sorted_paraules[-1], paraules, tauler)
    notIntersect = [word for word in paraules if word not in sorted_paraules]
    intersect.sort(key=lambda a : len(trobarInterseccions(a, notIntersect, tauler)))

    for paraula in intersect:
        if paraula not in sorted_paraules:
            sorted_paraules.append(paraula)
            recSortParaules(sorted_paraules, paraules, tauler)




def compleixRestriccions(tauler, diccionari, assignades):
    """
    Comprova que totes les paraules assignades compleixin les restriccions del
    problema, és a dir, que totes pertanyin al diccionari. També que no es
    repeteixi cap paraula

    Parameters
    ----------
    tauler : np.array
        Matriu numpy que representa el tauler de joc
    diccionari : dict
        Diccionari que té com a claus les longituds de les paraules i com a valors
        totes les paraules d'aquella longitud
    assignades : list
        Llista de paraules (Paraula) que ja han estat assignades
    
    Returns
    -------
    bool
        Retorna si les restriccions es compleixen
    """
    index = 0
    compleixRestriccions = True
    paraules = []

    while index < len(assignades) and compleixRestriccions:
        paraula = extreureParaula(tauler, assignades[index])

        if not np.any(np.all( paraula == diccionari[assignades[index].length], axis=1)):
            compleixRestriccions = False
        else:
            paraula = str(paraula)
            if paraula in paraules:
                compleixRestriccions = False
            else:
                paraules.append(paraula)
                index += 1
    return compleixRestriccions



def backTracking(noAssign, assign, tauler, diccionari):
    """
    Algorisme de Backtracking. Resol els mots encreuats passats com a parametre 
    amb les paraules del diccionari.

    Parameters
    ----------
    noAssign : list
        Llista de variables Paraula no assignades
    assign : list
        Llista de variables Paraula ja assignades
    tauler : np.array
        Matriu Numpy que representa el tauler de joc
    diccionari : dict
        Diccionari on les claus son longituds de paraula i els elements son
        numpy arrays amb les paraules del diccionari d'aquesta longitud
    
    Returns
    -------
    np.array
        Tauler solucionat en cas d'èxit. None en cas de que no existeixi solució
    """
    if len(noAssign) == 0:
        return tauler
    
    nouNoAssign = deepcopy(noAssign)
    nouAssign = deepcopy(assign)

    paraula = nouNoAssign.pop(0)
    nouAssign.append(paraula)

    for indexDomini in range(paraula.domini.shape[0]):
        nouTauler = deepcopy(tauler)
        nouTauler = assignaParaula(paraula, paraula.domini[indexDomini], nouTauler)

        if compleixRestriccions(nouTauler, diccionari, nouAssign):
            resultat = backTracking(nouNoAssign, nouAssign, nouTauler, diccionari)
            if resultat is not None:
                return resultat
    return None

def fwChecking(noAssign, assign, tauler, diccionari):
    """
    Algorisme de Backtracking amb Forward Checking. Resol els mots encreuats
    passats com a parametre amb les paraules del diccionari.

    Parameters
    ----------
    noAssign : list
        Llista de variables Paraula no assignades
    assign : list
        Llista de variables Paraula ja assignades
    tauler : np.array
        Matriu Numpy que representa el tauler de joc
    diccionari : dict
        Diccionari on les claus son longituds de paraula i els elements son
        numpy arrays amb les paraules del diccionari d'aquesta longitud
    
    Returns
    -------
    np.array
        Tauler solucionat en cas d'èxit. None en cas de que no existeixi solució
    """
    if len(noAssign) == 0:
        return tauler

    paraula = noAssign.pop(0)

    paraula.buildDomini(tauler, diccionari)
    for word in assign:
        if word.length == paraula.length:
            paraula.eliminaParaula(extreureParaula(tauler, word))

    assign.append(paraula)
    
    for indexDomini in range(paraula.domini.shape[0]):
        nouTauler = deepcopy(tauler)
        nouTauler = assignaParaula(paraula, paraula.domini[indexDomini], nouTauler)
        
        resultat = fwChecking(noAssign, assign, nouTauler, diccionari)

        if resultat is not None:
            return resultat

    noAssign.insert(0, assign.pop())

    return None


###############################################################################
#                             R E S O L U C I Ó                               #
###############################################################################


bckTime = time.time()
diccionari = llegirDiccionari("diccionari_CB.txt")
print("Temps de càrrega (Slow): " + str(time.time() - bckTime))

tauler = llegirTauler("crossword_CB.txt")
paraules = identificarParaules(tauler)

assignades = []
paraules = sortParaules(paraules, tauler)

assignades2 = []
paraules2 = deepcopy(paraules)
tauler2 = deepcopy(tauler)

testsBack = []
testsFw = []

for paraula in paraules:
    paraula.domini = diccionari[paraula.length]

for i in range(100):
    ################################
    # PROVA DE BACKTRACKING
    ################################

    bckTime = time.time()
    resultat1 = backTracking(deepcopy(paraules), [], tauler, diccionari)
    timer = time.time() - bckTime
    print("Temps de Resultat backTracking : " + str(timer))
    testsBack.append(timer)


    ################################
    # PROVA DE FORWARD-CHECKING
    ################################
    bckTime = time.time()
    resultat2 = fwChecking(deepcopy(paraules2), [], tauler2, diccionari)
    timer = time.time() - bckTime
    print("Temps de Resultat fwChecking : " + str(timer))
    testsFw.append(timer)



print("++++++ RESULTATS ++++++")
print("Mitjana de temps Bck: " + str(np.array(testsBack).sum()/len(testsBack)))
print("Solucio Bck: ")
for i in range(resultat1.shape[0]):
    for j in range(resultat1.shape[1]):
        print(resultat1[i][j], end=" ")
    print("")

print("+++++++++++++++++++++++")
print("Mitjana de temps Fwd: " + str(np.array(testsFw).sum()/len(testsFw)))
print("Solucio Fwd: ")
for i in range(resultat2.shape[0]):
    for j in range(resultat2.shape[1]):
        print(resultat2[i][j], end=" ")
    print("")